import { useContext } from 'react'
import { Navbar, Nav, Container } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar() {
	const { user } = useContext(UserContext)

	return (
		<Container>
			<Navbar bg="secondary" expand="lg">
				<Link href="/">
					<a className="navbar-brand text-light">XtraRise</a>
				</Link>

				<Navbar.Toggle aria-controls="navbar-content" />

				<Navbar.Collapse id="navbar-content">
					<Nav className="mr-auto">
						<Link href="/games">
							<a className="nav-link text-light" role="button">Game Dir.</a>
						</Link>
						{(user.isAdmin === true)
							?
							<Link href="/stats">
							<a className="nav-link text-light" role="button">Stats</a>
							</Link>
							:
							null
						}
					</Nav>

					{(user.email !== undefined || null)
						?
						<Nav className="ml-auto">
							<Link href="profile">
								<a className="nav-link text-light" role="button">Profile</a>
							</Link>

							<Link href="/logout">
								<a className="nav-link text-light" role="button">Logout</a>
							</Link>
						</Nav>
						:
						<Nav className="ml-auto">
							<Link href="/login">
								<a className="nav-link text-light" role="button">Login</a>
							</Link>

							<Link href="/register">
								<a className="nav-link text-light" role="button">Register</a>
							</Link>
						</Nav>
					}
				</Navbar.Collapse>
			</Navbar>
		</Container>
	)
}