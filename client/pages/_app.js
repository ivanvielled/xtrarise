import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { UserProvider } from '../UserContext'
import NavBar from '../components/NavBar'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles.css'


function MyApp({ Component, pageProps }) {
	const [gameId, setGameId] = useState('')

	const [user, setUser] = useState({
		id: null,
		email: null,
		username: null,
		isAdmin: null
	})

	useEffect(() => {
		const accessToken = localStorage.getItem('token')
		setGameId(localStorage.getItem('gameId')) //retain

		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/user-details`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setUser({
				id: data._id,
				email: data.email,
				username: data.username,
				isAdmin: data.isAdmin
			})
		})
	}, [user.id])

	const unsetUser = () => {
		localStorage.clear()

		setUser({
			id: null,
			email: null,
			username: null,
			isAdmin: null
		})
	}

  return (
  	<UserProvider value={{user, setUser, unsetUser, gameId}}>
  		<NavBar />
  		<Component {...pageProps} />
  	</UserProvider>
  ) 
}

export default MyApp
