import React, { useState, useEffect, useContext } from 'react'
import ReactDOM from 'react-dom'
import Head from 'next/head'
import View from '../components/View'
import UserContext from '../UserContext'
import { Container, Form, Button, Row, Col } from 'react-bootstrap'

export default function Home() {
	const { user } = useContext(UserContext)

	//set states for word and trigger for palindrome
	const [word, setWord] = useState('')
	const [isPalindrome, setIsPalindrome] = useState('')

	//function to check if the word is a palindrome
	function palindromeChecker(e) {
		e.preventDefault()

		//use Regular Expression to remove unwanted characters from the word
		const removeUnwanted = /[\W_]/g

		//lowercase the string
		const toLowerStr = word.toLowerCase().replace(removeUnwanted, '')

		//split the word, reerse the word, and join them with an empty string
		const reverseStr = toLowerStr.split('').reverse('').join('')

		//check if the reverseStr matches with the word
		if (reverseStr === word) {
			setIsPalindrome(true)
		} else {
			setIsPalindrome(false)
		}
	}
		
	useEffect(() => {
		if (word === '') {
			setIsPalindrome('')
		}
	}, [word])

	return (
    	<React.Fragment>
      		<View title={ 'XtraRise' }>
      			<Container>
	      			<div id="palindrome-container">
	      				<Form onSubmit={(e) => palindromeChecker(e)} className="text-center">
	      					<Form.Label className="mb-3"><b>Palindrome Checker</b></Form.Label>
		      				<Row>
		      					<Col xs={12} lg={3}>
		      						<h5>Type a word</h5>
		      					</Col>

		      					<Col xs={12} lg={9} className="pr-5">
		      						<Form.Control type="text" value={word} onChange={(e) => setWord(e.target.value)} />
		      					</Col>
		      				</Row>

		      				<Button className="btn btn-primary mt-5 mb-3" type="submit">Check</Button>
		      			</Form>

		      			<div className="text-center">
		      				{(isPalindrome === '')
		      				?
		      					null
		      				:
		      				(isPalindrome === true)
		      				?
		      					<span className="text-success text-center">The word is a Palindrome</span>
		      				:
			      				<span className="text-danger">The word is not a Palindrome</span>
		      				}
			      			</div>
		      		</div>
	      		</Container>
	     	</View>
	    </React.Fragment>
  	)
}