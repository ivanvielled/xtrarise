import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap'
import View from '../../components/View'
import UserContext from '../../UserContext'

export default function index() {
	const { user } = useContext(UserContext)

	const image = 'https://www.pinclipart.com/picdir/middle/5-52687_roman-clip-art-ancient-greek-spartan-helmet-png.png'

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [username, setUsername] = useState('')
	const [email, setEmail] = useState('')
	const [avatar, setAvatar] = useState('')

	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/user-details`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data !== null) {
			
			}
			
			setFirstName(data.firstName)
			setLastName(data.lastName)
			setUsername(data.username)
			setEmail(data.email)
			setAvatar(data.avatar)
		})
	}, [user.id])

	return (
		<View title={`${username}'s Profile`}>
			<Container>
				<Card>
					<Card.Header>{username}
					{(avatar !== null) 
						?
						<img className="avatar" src={avatar} />
						:
						<img className="avatar" src={image} />
					}
					</Card.Header>
					<Card.Body>
						<h5>{firstName}</h5>
						<h5>{lastName}</h5>
						<h5>{email}</h5>
					</Card.Body>
				</Card>
			</Container>
		</View>
	)
}