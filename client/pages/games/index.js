import React, { useState, useEffect, useContext } from 'react'
import { Card, Row, Col, Container, Tabs, Tab, Button, Form } from 'react-bootstrap'
import Router from 'next/router'
import Link from 'next/link'
import UserContext from '../../UserContext'
import View from '../../components/View'
import moment from 'moment'

export default function Index() {
	//user context
	const { user } = useContext(UserContext)

	//set state for active game records
	const [activeGameRecord, setActiveGameRecord] = useState([])

	//set state for inactive game records
	const [inactiveGameRecord, setInactiveGameRecord] = useState([])

	//search active games by name
	const [searchActive, setSearchActive] = useState('')

	//search inactive games by name
	const [searchInactive, setSearchInactive] = useState('')

	//check if user is Admin
	const [isAdmin, setIsAdmin] = useState('')

	//sort/unsort active games
	const [sortTypeActive, setSortTypeActive] = useState('asc')

	//sort/unsort inactive games
	const [sortTypeInactive, setSortTypeInactive] = useState('asc')

	// console.log(sortTypeActive)
	// console.log(sortTypeInactive)

	//check if user is admin
	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/user-details`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data.isAdmin === true) {
				setIsAdmin(true)
			} else {
				setIsAdmin(false)
			}
		})
	}, [user.id])

	//retrieve all available/active games
	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/games/active`, options)
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				if (searchActive !== null) {
					const gameSearchActive = data.filter(result => result.name.toLowerCase().includes(searchActive.toLowerCase()))
					setActiveGameRecord(gameSearchActive)
				}
			}
		})
	}, [searchActive])

	//retrieve all inactive games
	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/games/inactive`, options)
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				if (searchInactive !== null) {
					const gameSearchInactive = data.filter(result => result.name.toLowerCase().includes(searchInactive.toLowerCase()))
					setInactiveGameRecord(gameSearchInactive)

				}
			}
		})
	}, [searchInactive])

	//sort desc available/active games
	function sortDescActive(e) {
		e.preventDefault()
		
		setSortTypeActive('desc')
	}

	if (sortTypeActive === 'desc') {
		activeGameRecord.sort((a, b) => {
			if (a.name > b.name) {
				return 1
			} else if (a.name < b.name) {
				return -1
			} else {
				return 0
			}
		})
	}

	//sort desc inactive games
	function sortDescInactive(e) {
		e.preventDefault()

		setSortTypeInactive('desc')
	}

	if (sortTypeInactive === 'desc') {
		inactiveGameRecord.sort((a, b) => {
			if (a.name > b.name) {
				return 1
			} else if (a.name < b.name) {
				return -1
			} else {
				return 0
			}
		})
	}	

	//sort asc available/active games
	function sortAscActive(e) {
		e.preventDefault()

		setSortTypeActive('asc')
	}

	if (sortTypeActive === 'asc') {
		activeGameRecord.sort((a, b) => {
			if (a.name < b.name) {
				return 1
			} else if (a.name > b.name) {
				return -1
			} else {
				return 0
			}
		})
	}

	//sort asc inactive games
	function sortAscInactive(e) {
		e.preventDefault()

		setSortTypeInactive('asc')
	}

	if (sortTypeInactive === 'asc') {
		inactiveGameRecord.sort((a, b) => {
			if (a.name < b.name) {
				return 1
			} else if (a.name > b.name) {
				return -1
			} else {
				return 0
			}
		})
	}

	//list for active games
	const activeGameData = activeGameRecord.map(data => {
		// console.log(data)
		if (data.isActive === true) {
			return (
				<Card key={data._id} className="m-3 bg-dark">
					<Card.Header className="text-left"><h5 className="mt-3 ml-3 text-light">{data.name}</h5><img className="gameImg ml-4" src={data.image} /></Card.Header>
					<Card.Body>
						<p className="text-light">{data.description}</p>
						<p className="text-light">Price: ${data.price}</p>
						<p className="text-light">Qty: {data.qty}</p>
						<p className="text-light">Game URL:<br /> 
							<Link href={`${data.url}`}>
								<a target="_blank">{data.url}</a>
							</Link>
						</p>
						<div className="text-center">
							<Link href={`/games/game/${data._id}`}>
								<Button type="button" className="btn btn-primary">View {data.name}</Button>
							</Link>
						</div>
					</Card.Body>
				</Card>
			)
		}
	})
	
	const inactiveGameData = inactiveGameRecord.map(data => {
		// console.log(data)
		if (data.isActive === false) {
			return (
				<Card key={data._id} className="m-3 bg-dark">
					<Card.Header className="text-left"><h5 className="mt-3 ml-3 text-light">{data.name}</h5><img className="gameImg ml-4" src={data.image} /></Card.Header>
					<Card.Body>
						<p className="text-light">{data.description}</p>
						<p className="text-light">Price: ${data.price}</p>
						<p className="text-light">Qty: {data.qty}</p>
						<p className="text-light">Game URL:<br />
							<Link href={`${data.url}`}>
								<a target="_blank">{data.url}</a>
							</Link>
						</p>

						<div className="text-center">
							<Link href={`/games/game/${data._id}`}>
								<Button type="button" className="btn btn-primary">View {data.name}</Button>
							</Link>
						</div>
					</Card.Body>
				</Card>
			)
		}
	})

	return (
		<View title={ 'Game Directory' }>
			<Container>
			{isAdmin
				?
				<React.Fragment>
				<div className="text-right">
					<Link href="/games/create">
						<Button className="btn btn-primary">Add Game</Button>
					</Link>
				</div>

				<Tabs transition={false} id="gamesList-tab">
					<Tab eventKey="activeGames" title="Available Games">
						<div className="d-flex justify-content-center">
							<Row>
								<Col xs={12} md={10} className="text-left">
									<Form className="d-flex mt-3 mb-3">
										<Form.Group controlId="search">
											<Row>
												<Col xs={12} md={2}>
													<Form.Label>Search..</Form.Label>
												</Col>

												<Col xs={12} md={10}>
													<Form.Control type="search" autoComplete="off" value={searchActive} onChange={(e) => setSearchActive(e.target.value)} />
												</Col>
											</Row>
										</Form.Group>
									</Form>
								</Col>

								<Col xs={12} md={2}>
									<div className="text-center sortBtn">
									{(sortTypeActive === 'asc')
										?
										<Button type="button" onClick={(e) => sortDescActive(e)} className="btn btn-success mt-3 h-80">Sort A-Z</Button>
										:
										<Button type="button" onClick={(e) => sortAscActive(e)} className="btn btn-success mt-3 h-80">Sort Z-A</Button>
									}
									</div>
								</Col>
							</Row>
						</div>

						<Row>
							<Col xs={12} lg={12}>
								{activeGameData}
							</Col>
						</Row>
					</Tab>

					<Tab eventKey="inactiveGames" title="Inactive Games">
						<div className="d-flex justify-content-center">
							<Row>
								<Col xs={12} md={10} className="text-left">
									<Form className="d-flex mt-3 mb-3">
										<Form.Group controlId="search">
											<Row>
												<Col xs={12} md={2}>
													<Form.Label>Search..</Form.Label>
												</Col>

												<Col xs={12} md={10}>
													<Form.Control type="search" autoComplete="off" value={searchInactive} onChange={(e) => setSearchInactive(e.target.value)} />
												</Col>
											</Row>
										</Form.Group>
									</Form>
								</Col>

								<Col xs={12} md={2}>
									<div className="text-center sortBtn">
									{(sortTypeInactive === 'asc')
										?
										<Button type="button" onClick={(e) => sortDescInactive(e)} className="btn btn-success mt-3 h-80">Sort A-Z</Button>
										:
										<Button type="button" onClick={(e) => sortAscInactive(e)} className="btn btn-success mt-3 h-80">Sort Z-A</Button>
									}
									</div>
								</Col>
							</Row>
						</div>

						<Row>
							<Col xs={12} lg={12}>
								{inactiveGameData}
							</Col>
						</Row>
					</Tab>
				</Tabs>
				</React.Fragment>
				:
				<React.Fragment>
				<div className="d-flex justify-content-center">
					<Row>
						<Col xs={12} md={10} className="text-left">
							<Form className="d-flex mt-3 mb-3">
								<Form.Group controlId="search">
									<Row>
										<Col xs={12} md={2}>
											<Form.Label>Search..</Form.Label>
										</Col>

										<Col xs={12} md={10}>
											<Form.Control type="search" autoComplete="off" value={searchActive} onChange={(e) => setSearchActive(e.target.value)} />
										</Col>
									</Row>
								</Form.Group>
							</Form>
						</Col>

						<Col xs={12} md={2}>
							<div className="text-center sortBtn">
							{(sortTypeActive === 'asc')
								?
								<Button type="button" onClick={(e) => sortDescActive(e)} className="btn btn-success mt-3 h-80">Sort A-Z</Button>
								:
								<Button type="button" onClick={(e) => sortAscActive(e)} className="btn btn-success mt-3 h-80">Sort Z-A</Button>
							}
							</div>
						</Col>
					</Row>
				</div>


				<Row>
					<Col xs={12} md={4} lg={4}>
						{activeGameData}
					</Col>
				</Row>
				</React.Fragment>
			}
			</Container>
		</View>
	)
}