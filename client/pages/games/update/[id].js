import { useState, useEffect, useContext } from 'react'
import { Form, Row, Col, Container, Button, Card } from 'react-bootstrap'
import View from '../../../components/View'
import UserContext from '../../../UserContext'
import Link from 'next/link'
import { useRouter } from 'next/router'
import Router from 'next/router'

export default function Update() {
	//user context
	const { user } = useContext(UserContext)

	//used router component of nextjs to get params.gameId
	const router = useRouter()
	const gameId = router.query

	// console.log(gameId.id)

	//set initial states for object data
	const [id, setId] = useState('')
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [qty, setQty] = useState('')
	const [image, setImage] = useState('')
	const [url, setUrl] = useState('')

	//set state for admin check
	const [isAdmin, setIsAdmin] = useState('')

	//isActive trigger for button submit
	const [isActive, setIsActive] = useState(false)

	//use effect to set is active trigger and guard changes
	useEffect(() => {
		if (name !== '' && description !== '' && price !== '' && qty !== '' && image !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price, qty, image, url])

	//Retrieve user details
	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/user-details`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data.isAdmin === true) {
				setIsAdmin(true)
			} else {
				setIsAdmin(false)
			}
		})
	}, [user.id])

	// Retrieve the gameId from the URL
	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			}
		}

		// Use the retrieved gameId to send a GET request to the API
		// If gameId is not an empty string, fetch to API
		if (gameId !== '') {
			fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/${gameId.id}`, options)
			.then(res => res.json())
			.then(data => {
				// console.log(data)
				setId(data._id)
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
				setQty(data.qty)
				setImage(data.image)
				setUrl(data.url)
			})		
		}
	}, [gameId])

	//function update to update game
	function update(e) {
		e.preventDefault()

		const accessToken = localStorage.getItem('token')

		const options = {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			},
			body: JSON.stringify({
				gameId: id,
				name: name,
				description: description,
				image: image,
				price: price,
				qty: qty,
				url: url
			})
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/update`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			let gameId = data._id
			if (data !== false) {
				alert('Game successfully updated')
				Router.push('/games')
			} else {
				alert('Something went wrong')
			}
		})
	}

	return (
		<View title={ `Update ${name}` }>
			<Row className="justify-content-center">
				<Col md={6}>
					<Card>
						<Card.Header>Update {name}</Card.Header>

						<Card.Body>
							<Form onSubmit={(e) => update(e)}>
								<Form.Group controlId="name">
									<Form.Label>Name:</Form.Label>
									<Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} required autoComplete="off" />
								</Form.Group>

								<Form.Group controlId="description">
									<Form.Label>Description:</Form.Label>
									<Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)} required autoComplete="off" />
								</Form.Group>

								<Form.Group controlId="image">
									<Form.Label>Image<span className="gameImageSpan">*put image URL</span></Form.Label>
									<Form.Control type="text" value={ image } onChange={(e) => setImage(e.target.value)} autoComplete="off" required />
								</Form.Group>

								<Form.Group controlId="price">
									<Form.Label>Price</Form.Label>
									<Form.Control type="number" value={ price } onChange={(e) => setPrice(e.target.value)} autoComplete="off" required />
								</Form.Group>

								<Form.Group controlId="qty">
									<Form.Label>Qty.</Form.Label>
									<Form.Control type="number" value={ qty } onChange={(e) => setQty(e.target.value)} autoComplete="off" required />
								</Form.Group>

								<Form.Group controlId="url">
									<Form.Label>Game link:</Form.Label>
									<Form.Control type="text" value={ url } onChange={(e) => setUrl(e.target.value)} autoComplete="off" required />
								</Form.Group>

								{isActive ?
								    <Button variant="primary" type="submit" id="submitBtn">Save</Button>
								    :
								    <Button variant="primary" type="submit" id="submitBtn" disabled>Save</Button>
								}
							</Form>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</View>
	)
}