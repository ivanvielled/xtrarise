import React, { useState, useEffect, useContext } from 'react'
import { Form, Row, Col, Container, Button, Card } from 'react-bootstrap'
import View from '../../../components/View'
import UserContext from '../../../UserContext'
import Link from 'next/link'
import Router, { useRouter } from 'next/router'

export default function Game() {
	//user context
	const { user } = useContext(UserContext)

	//used router component of nextjs to get params.gameId
	const router = useRouter()
	const gameId = router.query

	// console.log(gameId.id)

	//set initial states for object data
	const [id, setId] = useState('')
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [qty, setQty] = useState('')
	const [image, setImage] = useState('')
	const [url, setUrl] = useState('')

	//set state for admin check
	const [isAdmin, setIsAdmin] = useState('')

	//check if game is active
	const [isActive, setIsActive] = useState('')

	//Retrieve user details
	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/user-details`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data.isAdmin === true) {
				setIsAdmin(true)
			} else {
				setIsAdmin(false)
			}
		})
	}, [user.id])

	// Retrieve the gameId from the URL
	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			}
		}

		// Use the retrieved gameId to send a GET request to the API
		// If gameId is not an empty string, fetch to API
		if (gameId !== '') {
			fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/${gameId.id}`, options)
			.then(res => res.json())
			.then(data => {
				// console.log(data)
				setId(data._id)
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
				setQty(data.qty)
				setImage(data.image)
				setUrl(data.url)

				if (data.isActive === true) {
					setIsActive(true)
				} else {
					setIsActive(false)
				}
			})
		}
	}, [gameId])

	//function to archive game
	function archive(e) {
		e.preventDefault()

		const accessToken = localStorage.getItem('token')

		const options = {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			},
			body: JSON.stringify({
				gameId: id,
				isActive: false
			})
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/archive`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if (data !== false) {
				alert('Game has been archived')
				Router.push('/games')
			} else {
				alert('Something went wrong')
			}
		})
	}

	//function to reactivate game
	function reactivate(e) {
		e.preventDefault()

		const accessToken = localStorage.getItem('token')

		const options = {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			},
			body: JSON.stringify({
				gameId: id,
				isActive: true
			})
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/reactivate`, options)
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if (data === true) {
				alert('Game has been reactivated')
				Router.push('/games')
			} else {
				alert('Something went wrong')
			}
		})
	}

	// console.log(id)

	function deleteGame(e) {
		e.preventDefault()

		const accessToken = localStorage.getItem('token')

		const options = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			}
		}

		const payload = {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/${id}`, options)
		.then(res => res.json())
		.then(data => {
			if (data !== undefined) {
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/delete/${data._id}`, payload)
				.then(res => res.json())
				.then(data => {
					if (data !== undefined) {
						alert('Game has been deleted')
						Router.push('/games')
					} else {
						alert('Something went wrong')
					}
				})
			} 
		})
	}

	return (
		<View title={ `${name}` }>
			<Link href="/games">
				<a>Go back to games</a>
			</Link>
			<Row className="justify-content-center mt-5">
				<Col md={6}>
					<Card className="bg-dark">
						<Card.Header className="d-flex"><h5 className="text-center ml-5 mr-5 mt-3 text-light">{name}</h5><img className="gameImg" src={image} /></Card.Header>

						<Card.Body>
							<p className="text-light">{description}</p>
							<p className="text-light">Price: ${price}</p>
							<p className="text-light">Qty: {qty}</p>
							<p className="text-light">Game URL:<br />
								<Link href={`${url}`}>
									<a target="_blank">{url}</a>
								</Link>
							</p>
							{isAdmin
								?
								isActive
									?
									<div id="btnGrpAdmin" className="text-center d-flex justify-content-center">
										<Link href={`/games/update/${id}`}>
											<Button className="btn btn-success">Update {name}</Button>
										</Link>

										<Form onSubmit={(e) => archive(e)}>
											<Button type="submit" className="btn btn-danger ml-3">Archive {name}</Button>
										</Form>
									</div>
									:
									<div className="text-center d-flex justify-content-center">
										<Form onSubmit={(e) => reactivate(e)}>
											<Button type="submit" className="btn btn-primary">Retrieve {name}</Button>
										</Form>

										<Form onSubmit={(e) => deleteGame(e)}>
											<Button type="submit" className="btn btn-danger ml-3">Delete {name} (permanently)</Button>
										</Form>
									</div>
								: 
								<React.Fragment>
									<div className="text-center">
										<Button className="btn btn-success">Buy</Button>
									</div>
								</React.Fragment>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</View>
	)
}