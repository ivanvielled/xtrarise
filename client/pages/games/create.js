import { useState, useEffect, useContext } from 'react'
import { Card, Col, Row, Container, Form, Button } from 'react-bootstrap'
import Router from 'next/router'
import View from '../../components/View'
import UserContext from '../../UserContext'

export default function Create() {
	return (
		<View title={ 'Add Game' }>
			<Row className="justify-content-center">
				<Col md={6}>
					<h3 className="text-center">Add Game</h3>
					<AddGameForm />
				</Col>
			</Row>
		</View>
	)
}

const AddGameForm = () => {
	const { user } = useContext(UserContext)

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [qty, setQty] = useState(0)
	const [image, setImage] = useState('')
	const [url, setUrl] = useState('')

	const [isActive, setIsActive] = useState(false)

	function addGame(e) {
		e.preventDefault()

		const accessToken = localStorage.getItem('token')

		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${accessToken}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				qty: qty,
				image: image,
				url: url
			})
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/games/add-game`, options)
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				alert('Game successfully added to directory')

				Router.push('/games')
			} else {
				alert('Something went wrong')
			}
		})

		setName('')
		setDescription('')
		setPrice(0)
		setQty(0)
		setImage('')
		setUrl('')
	}

	useEffect(() => {
		if (name !== '' && description !== '' && price !== '' && qty !== '' && image !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price, qty, image, url])

	return (
		<Container className="mt-3">
			<Card>
				<Card.Header>Add Game</Card.Header>
				<Card.Body>
					<Form onSubmit={(e) => addGame(e)}>
						<Form.Group controlId="name">
							<Form.Label>Game name</Form.Label>
							<Form.Control type="text" value={ name } onChange={(e) => setName(e.target.value)} autoComplete="off" required />
						</Form.Group>

						<Form.Group controlId="description">
							<Form.Label>Game description</Form.Label>
							<Form.Control type="text" value={ description } onChange={(e) => setDescription(e.target.value)} autoComplete="off" required />
						</Form.Group>

						<Form.Group controlId="image">
							<Form.Label>Image<span className="gameImageSpan">*put image URL</span></Form.Label>
							<Form.Control type="text" value={ image } onChange={(e) => setImage(e.target.value)} autoComplete="off" required />
						</Form.Group>

						<Form.Group controlId="price">
							<Form.Label>Price</Form.Label>
							<Row className="d-flex">
								<Col md={1}>
									<span>$</span>
								</Col>

								<Col md={11}>
									<Form.Control type="number" value={ price } onChange={(e) => setPrice(e.target.value)} autoComplete="off" required />
								</Col>
							</Row>
						</Form.Group>

						<Form.Group controlId="qty">
							<Form.Label>Qty.</Form.Label>
							<Form.Control type="number" value={ qty } onChange={(e) => setQty(e.target.value)} autoComplete="off" required />
						</Form.Group>

						<Form.Group controlId="url">
							<Form.Label>Game link:</Form.Label>
							<Form.Control type="text" value={ url } onChange={(e) => setUrl(e.target.value)} autoComplete="off" required />
						</Form.Group>
						

						{isActive ?
						    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
						    :
						    <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
						}
					</Form>
				</Card.Body>
			</Card>
		</Container>
	)
} 