import { useState, useEffect } from 'react'
import { Form, Button, Card, Row, Col, Container } from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../../UserContext'
import View from '../../components/View'

export default function index() {
	return (
		<View title={ 'Register' }>
			<Row className="justify-content-center">
	            <Col xs md="6">
	                <h3 className="text-center">Register</h3>
	                <RegisterForm/>
	            </Col>
	        </Row>
		</View>
	)
}

const RegisterForm = () => {

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [username, setUsername] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	const [isActive, setIsActive] = useState(false)
	const [passwordMatched, setPasswordMatched] = useState('')

	useEffect(() => {
		if ((username !== '' && password1 !== '' && password2 !== '')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2])

	useEffect(() => {
		if (password1 === password2) {
			setPasswordMatched(true)
		} else {
			setPasswordMatched(false)
		}
	}, [password1, password2])

	function register(e) {
		e.preventDefault()

		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				username: username,
				email: email,
				password: password1
			})
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/register`, options)
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

		setFirstName('')
		setLastName('')
		setUsername('')
		setEmail('')
		setPassword1('')
		setPassword2('')

		alert('Registration successful!')

		Router.push('/login')
	}

	return (
		<Container className="mt-3">
			<Card>
				<Card.Header>Register</Card.Header>
				<Card.Body>
					<Form onSubmit={(e) => register(e)}>
						<Form.Group controlId="firstName">
			    		    <Form.Label>First Name</Form.Label>
			    		    <Form.Control type="text" value={ firstName } onChange={ (e) => setFirstName(e.target.value) } autoComplete="off" required/>
			    		</Form.Group>
				    	
				    	<Form.Group controlId="lastName">
			    		    <Form.Label>Last Name</Form.Label>
			    		    <Form.Control type="text" value={ lastName } onChange={ (e) => setLastName(e.target.value) } autoComplete="off" required/>
			    		</Form.Group>

			    		<Form.Group controlId="username">
			    		    <Form.Label>Username</Form.Label>
			    		    <Form.Control type="text" value={ username } onChange={ (e) => setUsername(e.target.value) } autoComplete="off" required/>
			    		</Form.Group>

			            <Form.Group controlId="userEmail">
			                <Form.Label>Email address</Form.Label>
			                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
			                <Form.Text className="text-muted">
			                We'll never share your email with anyone else.
			                </Form.Text>
			            </Form.Group>

			            <div className="d-block mb-3">
			            	<Form.Group controlId="password1">
			            	    <Form.Label>Password</Form.Label>
			            	    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
			            	</Form.Group>

			            	<Form.Group controlId="password2">
			            	    <Form.Label>Verify Password</Form.Label>
			            	    <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
			            	</Form.Group>

			            	{passwordMatched 
			            		?
			            		<span className="pwConfirm-true">Password matched!</span>
			            		:
			            		<span className="pwConfirm-false">Password does not match</span>
			            	}
			            </div>

			            {isActive ?
			                <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			                :
			                <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
			            }
					</Form>
				</Card.Body>
			</Card>
		</Container>
	)
}