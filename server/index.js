const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')

require('dotenv').config()

const port = 4000

//routes
const userRoutes = require('./routes/user')
const gameRoutes = require('./routes/game')

//whitelist to enable api use
const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
}

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))
mongoose.connect('mongodb+srv://mongodbcluster:mongodbcluster@cluster0.ceomo.mongodb.net/XtraRise?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

app.use(express.json({ limit: '5mb' }))
app.use(express.urlencoded({ extended: true }))

app.use('/api/users', cors(corsOptions), userRoutes)
app.use('/api/games', cors(corsOptions), gameRoutes)

app.listen(port, () => {
	console.log(`Server is now online at port ${port}`)
})