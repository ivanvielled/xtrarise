const mongoose = require('mongoose')
const moment = require('moment')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},

	username: {
		type: String,
	},

	email: {
		type: String,
		required: [true, 'Email is required']
	},

	password: {
		type: String
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	loginType: {
		type: String,
		required: [true, 'Login Type is required']
	},

	avatar: {
		type: String,
	},

	games: [{
		gameId: {
			type: String,
			required: [true, 'Game Id is required']
		},

		addedOn: {
			type: Date,
			default: moment()
		},

		status: {
			type: String,
			default: "Added"
		}
	}]
})

module.exports = mongoose.model("User", userSchema)