const mongoose = require('mongoose')
const moment = require('moment')

const gameSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Game name is required"]
	},

	description: {
		type: String,
		required: [true, "Game description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	qty: {
		type: Number,
		required: [true, "Quantity is required"]
	},

	image: {
		type: String
	},

	url: {
		type: String,
	},

	buyers: [{
		userId: {
			type: String,
			required: [true, "User Id is required"]
		},

		boughtOn: {
			type: Date,
			default: moment()
		}
	}]
})

module.exports = mongoose.model("Game", gameSchema)