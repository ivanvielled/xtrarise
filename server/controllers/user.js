const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const { OAuth2Client } = require('google-auth-library')
const clientId = '626126006176-mrh7gvkr4624mfisq6fec6cjjr9eu2ar.apps.googleusercontent.com'

//Login&Registration

//check if email already exists
module.exports.emailExists = (params) => {
	return User.find({
		email: params.email
	})
	.then(resultFromEmailExist => {
		return resultFromEmailExist.length > 0 ? true : false
	})
}

//register user(email)
module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		username: params.username,
		email: params.email,
		avatar: null,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'email'
		//hashSync() encrypts the password and 10 makes it happen 10 times
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
	})
}

//login using the username
module.exports.login = (params) => {
	return User.findOne({
		username: params.username
	})
	.then(user => {
		if (user === null) {
			return {
				error: 'does-not-exist'
			}
		}

		if (user.loginType !== 'email') {
			return {
				error: 'login-type-error'
			}
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
		//compare non-hashed passwords to hashed passwords using compareSync()

		if (isPasswordMatched) {
			return {
				accessToken: auth.createAccessToken(user.toObject())
			}
				//creates access token and turn to JSON format
		} else {
			return {
				error: 'incorrect-password'
			}
		}
	})
}

//get user details
module.exports.getUser = (params) => {
	return User.findById(params.userId)
	.then(user => {
		user.password = undefined 
		//display password as undefined

		return user
	})
}

//buy a game
module.exports.buyGame = (params) => {
	return User.findById(params.userId).then(resultFromBuyGame => {
		resultFromBuyGame.games.push({
			gameId: params.gameId
		}) //pushes the gameId to the user's games array(game is now bought)

		return resultFromBuyGame.save().then((resultFromSaveUser, err) => {
			return Game.findById(params.gameId)
			.then(resultFromFindByIdGame => {
				resultFromFindByIdGame.buyers.push({
					userId: params.userId
				}) //adds a buyer userId to buyers array

				return resultFromFindByIdGame.save()
				.then((resultFromBuyGame, err) => {
					return (err) ? false : true
				})
			})
		})
	})
}


//google login
module.exports.googleTokenId = async (tokenId) => {
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})

	if (data.payload.email_verified === true) {
		const user = await User.findOne({
			email: data.payload.email
		}).exec()

		if (user !== null) {
			if (user.loginType === 'google') {
				return {
					accessToken: auth.createAccessToken(user.toObject())
				}
			} else {
				return {
					error: 'login-type-error'
				}
			}
		} else {
			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				username: data.payload.given_name,
				email: data.payload.email,
				avatar: null,
				loginType: "google"
			})

			return newUser.save().then((user, err) => {
				return {
					accessToken: auth.createAccessToken(user.toObject())
				}
			})
		}
	} else {
		return {
			error: 'google-auth-error'
		}
	} 
}
