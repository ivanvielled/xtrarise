const Game = require('../models/game')

//view all available games
module.exports.getAllActive = () => {
	return Game.find({
		isActive: true
	})
	.then(resultsFromFind => resultsFromFind)
}

//view all archived games
module.exports.getAllInactive = () => {
	return Game.find({
		isActive: false
	})
	.then(resultsFromFind => resultsFromFind)
}

//create/add games
module.exports.add = (params) => {
	let newGame = new Game({
		name: params.name,
		description: params.name,
		price: params.price,
		qty: params.qty,
		image: params.image,
		url: params.url
	})

	return newGame.save()
	.then((game, err) => {
		return (err) ? false : true
	})
}

//get a specific game
module.exports.getGame = (params) => {
	return Game.findById(params.gameId)
	.then(game => game)
}

//modify game details
module.exports.update = (params) => {
	let updatedGame = {
		name: params.name,
		description: params.description,
		price: params.price,
		qty: params.qty,
		image: params.image,
		url: params.url
	}

	return Game.findByIdAndUpdate(params.gameId, updatedGame)
	.then((game, err) => {
		return (err) ? false : true
	})
}

//archive a game (softDelete)
module.exports.archive = (params) => {
	let updateActive = {
		isActive: false
	}

	return Game.findByIdAndUpdate(params.gameId, updateActive)
	.then((game, err) => {
		return (err) ? false : true
	})
}

//reactivate a game
module.exports.reactivate = (params) => {
	let reactivateActive = {
		isActive: true
	}

	return Game.findByIdAndUpdate(params.gameId, reactivateActive)
	.then((game, err) => {
		return (err) ? false : true
	})
}

//delete a game(permanently)
module.exports.deleteGame = (params) => {
	return Game.deleteOne({
		_id: params.gameId
	})
	.then(resultFromDelete => resultFromDelete)
}