const express = require('express')
const router = express.Router()
const auth = require('../auth')
const GameController = require('../controllers/game')

//get all available games
router.get('/games/active', (req, res) => {
	GameController.getAllActive()
	.then(resultsFromFind => res.send(resultsFromFind))
})

//get all unavailable games
router.get('/games/inactive', (req, res) => {
	GameController.getAllInactive()
	.then(resultsFromFind => res.send(resultsFromFind))
})

//get a specific game
router.get('/:gameId', (req, res) => {
	let gameId = req.params.gameId

	GameController.getGame({
		gameId
	})
	.then(resultFromFindById => res.send(resultFromFindById))
})

//create/add games
router.post('/add-game', auth.verify, (req, res) => {
	GameController.add(req.body)
	.then(resultFromAdd => res.send(resultFromAdd))
})

//update a game
router.put('/update', auth.verify, (req, res) => {

	GameController.update(req.body)
	.then(resultFromUpdate => res.send(resultFromUpdate))
})

//archive a game(soft delete)
router.put('/archive', auth.verify, (req, res) => {

	GameController.archive(req.body)
	.then(resultFromArchive => res.send(resultFromArchive))
})

//delete a game(permanently)
router.delete('/delete/:gameId', auth.verify, (req, res) => {
	let gameId = req.params.gameId

	GameController.deleteGame({
		gameId
	})
	.then(resultFromDelete => res.send(resultFromDelete))
})

//retrieve a game(set is active to true)
router.put('/reactivate', auth.verify, (req, res) => {

	GameController.reactivate(req.body)
	.then(resultFromReactivate => res.send(resultFromReactivate))
})

module.exports = router