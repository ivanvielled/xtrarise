const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

//check if email exists
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body)
	.then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//registration(email)
router.post('/register', (req, res) => {
	UserController.register(req.body)
	.then(resultFromRegister => res.send(resultFromRegister))
})

//login(username)
router.post('/login', (req, res) => {
	UserController.login(req.body)
	.then(resultFromLogin => res.send(resultFromLogin))
})

//get user details
router.get('/user-details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)

	UserController.getUser({
		userId: user.id
	})
	.then(user => res.send(user))
})

//verify google login token
router.post('/login-google', async (req, res) => {
	res.send(await UserController.googleTokenId(req.body.tokenId))
})

//buy game
router.put('/buy-game', auth.verify, (req, res) => {
	let params = {
		userId: auth.decode(req.headers.authorization).id,
		gameId: req.body.gameId
	}

	UserController.buyGame(params)
	.then(resultFromBuyGame => res.send(resultFromBuyGame))
})

module.exports = router