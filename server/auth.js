const jwt = require('jsonwebtoken')
const secret = "xtrarise"

//create access token
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		username: user.username,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, { expiresIn: '24h' })
}

//verify jwt of request
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send({ auth: "failed" }) : next()
		})
	} else {
		return res.send({ auth: "failed" })
	}
}

//decode token
module.exports.decode = (token) => {
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, { complete: true })
			.payload
		})
	}
}